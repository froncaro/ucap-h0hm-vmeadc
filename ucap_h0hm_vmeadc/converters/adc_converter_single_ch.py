import logging
from typing import List
import numpy as np

from ucap_h0hm_vmeadc.process_adc import get_stats, get_around_peak, running_average

from ucap.common import (
    AcquiredParameterValue,
    Event,
    FailSafeParameterValue,
    ValueHeader,
    ValueType,
)
from ucap.common.context import (
    configuration,
    device_name,
    transformation_name,
    published_property_name,
)

logger = logging.getLogger(__name__)

###def convert(event: Event) -> Optional[FailSafeParameterValue]:
def convert(event: Event) -> List[FailSafeParameterValue]:
    #this is a test 4
    logger = logging.getLogger(__name__)
    #logger.info("testing  %s/%s", device_name, transformation_name)
    #logger.info("this is a test")
    header = event.trigger_value.header if event.trigger_value else ValueHeader()
    #logger.info("Got Header ")

    beam_intensity_bct10 = event.get_value("BCT10").value.get_value("totalIntensity")
    beam_intensity_bct20 = event.get_value("BCT20").value.get_value("totalIntensity")

    beam_intensity_ring = event.get_value("BCT_RING").value.get_value("hotSpotsSamples") #3 samples: @Inj, @Capture, @Ej
    beam_intensity_at_inj = beam_intensity_ring[0][0]

    psb_user = event.get_value("USER").value.get_value()
    print ("USER=",psb_user)
    #logger.info("USER , The BEAM INTENSITY from the BCT10 is : %1.3f", beam_intensity_bct10)
    #logger.info("USER , The BEAM INTENSITY from the BCT20 is : %1.3f", beam_intensity_bct20)
    # if bct_int <= configuration['threshold_BCT']:
    #    return []

    #output_apv = AcquiredParameterValue(f'{device_name}/ADC_data', header)



    adc_apv: AcquiredParameterValue = event.get_value("adc_acquisition").value
    channel = configuration['channel']
    pname = configuration['channel_name']

    data, type_data = adc_apv.get_value_and_type(channel)

    clock_divider, type_divider = adc_apv.get_value_and_type('adcClockDividerSIS33')
    tsampling = 1. / (100e6 / clock_divider)

    #adc_ch1, type_data = adc_apv.get_value_and_type("dataChannel1")

    #logger.info("\nCHECK --- %s\n", type_data)

    #if len(pos) != len(proj):
    #    logger.warning(f"Sem {id_sem} contained inconsistent data")
    #    return []


    output_apv_i = AcquiredParameterValue(f'{device_name}/interlockSignal', header)
    output_apv = AcquiredParameterValue(f'{device_name}/processedData', header)
    #logger.info("LOGGINH HERE CH NAME= %s ",pname)

    output_apv_i.update_value(f"data", data, ValueType.SHORT_ARRAY)
    #output_apv.update_value(f"data", data.tolist(), ValueType.INT_ARRAY)
    #logger.info('DATA=%f',data)
    data = np.array(data)
    if not len(data):
        logger.info('NO DATA !!!!!!!!!!!!!!!!!!!!!!!!  ')
        return FailSafeParameterValue('NO DATAAAAAAAAAAAAAAAAAAAAAAAAAAA')

    ymin, ymax, ymean, offset = get_stats(data)
    imin, v_around_peak = get_around_peak(data)

    try:
        absolute_peak  = ( ymin - offset )
        relative_peak = abs(absolute_peak) / abs(ymean - offset)
    except:
        absolute_peak, relative_peak = 0,0



    w = 5000
    data_averaged = running_average(data, w)[::w].astype(int) - offset

    try:
        #relative_transmission = data_averaged.sum() /  beam_intensity_bct20[0]
        relative_transmission = beam_intensity_at_inj / beam_intensity_bct20[0]
    except:
        relative_transmission = 0

    #logger.info("DATA MIN,MAX,MEAN = %d\t%d\t%d", ymin, ymax, ymin)

    #stats = [ymin, ymax, ymean]

    #output_apv.update_value( f"{pname}_stats", stats, ValueType.INT_ARRAY )

    output_apv.update_value(f"tsampling",np.round( float(tsampling)*1e6, 3 ), ValueType.FLOAT)
    output_apv.update_value(f"tsampling_lowrate", np.round(float(tsampling * w*1e6),3), ValueType.FLOAT)
    output_apv.update_value(f"min", int(ymin), ValueType.INT )
    output_apv.update_value(f"max", int(ymax), ValueType.INT)
    output_apv.update_value(f"mean", int(ymean), ValueType.INT)
    output_apv.update_value(f"offset", int(offset), ValueType.INT)
    output_apv.update_value(f"min_value_index", int(imin), ValueType.INT)

    output_apv.update_value(f"absolute_peak", float(absolute_peak), ValueType.FLOAT)
    output_apv.update_value(f"relative_peak", float(relative_peak), ValueType.FLOAT)
    output_apv.update_value(f"relative_transmision", float(relative_transmission), ValueType.FLOAT)

    if absolute_peak < -50:
        output_apv.update_value(f"data_around_minimum", v_around_peak.tolist(), ValueType.INT_ARRAY)
        output_apv.update_value(f"data_lowrate", data_averaged.tolist(), ValueType.INT_ARRAY)
    else:
        output_apv.update_value(f"data_around_minimum", [], ValueType.INT_ARRAY)
        output_apv.update_value(f"data_lowrate", [], ValueType.INT_ARRAY)

    ###
    user_apv = AcquiredParameterValue(f'{device_name}/User', header)
    user_apv.update_value(f"USER", psb_user, ValueType.STRING)
    ###
    bct_apv = AcquiredParameterValue(f'{device_name}/BCT', header)

    '''
    logger.info("BI.BCT10\n")
    for ii, v in enumerate( beam_intensity_bct10):
        logger.info("%d Value = %f\n", ii, v )
    logger.info("BI.BCT20\n")
    for ii, v in enumerate( beam_intensity_bct20):
        logger.info("%d Value = %f\n", ii, v )
    '''
    bct_apv.update_value("BI.BCT10", beam_intensity_bct10[0], ValueType.FLOAT)
    bct_apv.update_value("BI.BCT20", beam_intensity_bct20[0], ValueType.FLOAT)
    bct_apv.update_value("BR.BCT",   beam_intensity_at_inj,     ValueType.FLOAT)
    ########
    f_output_apv_i = FailSafeParameterValue(output_apv_i)
    f_output_apv = FailSafeParameterValue(output_apv)
    f_user_apv = FailSafeParameterValue(user_apv)
    f_bct_apv = FailSafeParameterValue(bct_apv)

    return [f_output_apv_i, f_output_apv, f_user_apv, f_bct_apv]

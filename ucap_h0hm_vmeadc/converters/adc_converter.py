import logging
from typing import List
import numpy as np

from ucap_h0hm_vmeadc.process_adc import get_stats, get_around_peak, running_average

from ucap.common import (
    AcquiredParameterValue,
    Event,
    FailSafeParameterValue,
    ValueHeader,
    ValueType,
)
from ucap.common.context import (
    configuration,
    device_name,
    transformation_name,
    published_property_name,
)

logger = logging.getLogger(__name__)

###def convert(event: Event) -> Optional[FailSafeParameterValue]:
def convert(event: Event) -> List[FailSafeParameterValue]:
    #this is a test 4
    logger = logging.getLogger(__name__)
    logger.info("testing  %s/%s", device_name, transformation_name)
    logger.info("this is a test")
    header = event.trigger_value.header if event.trigger_value else ValueHeader()
    logger.info("Got Header ")

    beam_intensity = event.get_value("BCT").value.get_value("totalIntensity")
    psb_user = event.get_value("USER").value.get_value()
    print ("USER=",psb_user)
    logger.info("USER , The BEAM INTENSITY from the BCT is : %1.3f", beam_intensity)
    # if bct_int <= configuration['threshold_BCT']:
    #    return []

    #output_apv = AcquiredParameterValue(f'{device_name}/ADC_data', header)



    adc_apv: AcquiredParameterValue = event.get_value("adc_acquisition").value
    adc_ch1, type_data = adc_apv.get_value_and_type("dataChannel1")
    logger.info("\nCHECK --- %s\n", type_data)
    adc_ch2, type_data = adc_apv.get_value_and_type("dataChannel2")
    adc_ch3, type_data = adc_apv.get_value_and_type("dataChannel3")
    adc_ch4, type_data = adc_apv.get_value_and_type("dataChannel4")



    #if len(pos) != len(proj):
    #    logger.warning(f"Sem {id_sem} contained inconsistent data")
    #    return []
    lacq_data = [adc_ch1,adc_ch2,adc_ch3,adc_ch4]
    loutput_apv = []

    w = 1000
    for ich, ch in enumerate( configuration["channels"]):
        pname = configuration["channel_names"][ich]

        output_apv = AcquiredParameterValue(f'{device_name}/{pname}', header)

        logger.info("LOGGINH HERE CH NAME= %s ",pname)
        data=lacq_data[ich]
        output_apv.update_value(pname, data, ValueType.SHORT_ARRAY)

        data = np.array(data)

        ymin, ymax, ymean, offset = get_stats(data)
        imin, v_around_peak = get_around_peak(data)

        data_averaged = running_average(data, w)[::w].astype(int) - offset

        logger.info("DATA MIN,MAX,MEAN = %d\t%d\t%d", ymin, ymax, ymin)

        #stats = [ymin, ymax, ymean]

        #output_apv.update_value( f"{pname}_stats", stats, ValueType.INT_ARRAY )
        output_apv.update_value(f"min", int(ymin), ValueType.INT )
        output_apv.update_value(f"max", int(ymax), ValueType.INT)
        output_apv.update_value(f"mean", int(ymean), ValueType.INT)
        output_apv.update_value(f"offset", int(offset), ValueType.INT)
        output_apv.update_value(f"min_value_index", int(imin), ValueType.INT)


        output_apv.update_value(f"data_around_minimum", v_around_peak.tolist(), ValueType.INT_ARRAY)


        output_apv.update_value(f"data_lowrate", data_averaged.tolist(), ValueType.INT_ARRAY)

        loutput_apv.append(FailSafeParameterValue(output_apv))

    ###
    user_apv = AcquiredParameterValue(f'{device_name}/User', header)
    user_apv.update_value("USER", psb_user, ValueType.STRING)
    ###
    bct_apv = AcquiredParameterValue(f'{device_name}/BCT', header)
    print(beam_intensity)
    logger.info("BCT.BI10\n")
    for ii, v in enumerate( beam_intensity):
        logger.info("%d Value = %f\n", ii, v )

    bct_apv.update_value("BI.BCT10", beam_intensity[0], ValueType.FLOAT)

    f_output_apv = FailSafeParameterValue(output_apv)
    f_user_apv = FailSafeParameterValue(user_apv)
    f_bct_apv = FailSafeParameterValue(bct_apv)

    return [f_output_apv, f_user_apv, f_bct_apv] + loutput_apv

import numpy as np

def get_stats(raw_data):
    ymin = raw_data.min()
    ymax = raw_data.max()
    ymean = raw_data.mean()

    offset = raw_data[:500].mean()

    return int(ymin),  int(ymax), int(ymean), int(offset)


def get_around_peak(raw_data):
    imin = np.argmin(raw_data)

    return imin, raw_data[imin-50:imin+50].astype(int)

def running_average(raw_data, w):
    return np.convolve(raw_data, np.ones(w), 'valid') / w